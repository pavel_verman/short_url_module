<?php

namespace app\models;

use Yii;
use yii\helpers\BaseUrl;

/**
 * This is the model class for table "short_urls".
 *
 * @property string $id
 * @property string $long_url
 * @property string $short_code
 * @property string $date_created
 */
class ShortUrl extends \yii\db\ActiveRecord
{
    protected static $CODING_ALPHABET = "1234567890ABCDEFGHJKLMNPQRSTVWXYZ";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'short_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['long_url'], 'required'],
            [['date_created'], 'integer'],
            [['long_url'], 'string', 'max' => 255],
            [['short_code'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'long_url' => 'Long Url',
            'short_code' => 'Short Code',
            'date_created' => 'Date Created',
        ];
    }

    public function saveNewRecord()
    {
        $this->save(false);
        $this->generateUrlCode();
        $this->setCreateDate();
        $this->save();
    }

    protected function generateUrlCode()
    {
        // Generate new url based on the record-id in the database with charset $CODING_ALPHABET
        $cur_id = $this->getPrimaryKey();
        $length = strlen(self::$CODING_ALPHABET);
        $code = "";
        while ($cur_id > 0) {
            $code = self::$CODING_ALPHABET[$cur_id % $length] . $code;
            // Integer division in PHP
            $cur_id = (int)($cur_id / $length);
        }
        return $this->short_code = $code;
    }

    protected function setCreateDate()
    {
        $this->date_created = strtotime(date('Y-m-d H:i:s'));
    }

    public function addBaseUrl()
    {
        $this->short_code = BaseUrl::to('/' . $this->short_code, 'http') ;
    }
}
