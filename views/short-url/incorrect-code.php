<?php
/* @var $this yii\web\View */
?>
<?php
use yii\helpers\Html;
?>
<header><h1>Возникли проблемы со ссылкой <b><?=Html::encode($url_code)?></b>  =(</h1></header>

<section>
    <br>
    <h2>Попробуйте другую или <?= Html::a('создайте новую', '/') ?></h2>
</section>
