<?php
/* @var $this yii\web\View */
?>
<header><h1>Укороти свой URL!</h1></header>

<section>
    <?php
    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;

     Pjax::begin([
        // Pjax options
    ]);
        $form = ActiveForm::begin([
            'id' => 'short-url-form',
            'options' => ['class' => 'simple-form', 'data' => ['pjax' => true]],
        ]) ?>
        <?= $form->field($model, 'long_url')->label('Введите ваш url')?>
        <?= Html::submitButton('Укоротить', ['class' => 'btn-generate']) ?>
    <div id="generated-code">
        <br>
        <p>
        <?=Html::label("Укороченный url:")?>
        <?= Html::label($model->getAttribute('short_code'),
            'short-url-form', ['class' => 'short-code-label']); ?>
        </p>
        <p>
            <?= Html::a('Перейти', $model->getAttribute('short_code')); ?>
        </p>
    </div>
        <?php ActiveForm::end();
    pjax::end(); ?>
</section>