<?php

namespace app\controllers;

use Yii;
use app\models\ShortUrl;
use yii\helpers\Url;

class ShortUrlController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ShortUrl();
        // correct entry is completely. given parameter: long_url
        if ($model->load(Yii::$app->request->post())) {
            //TODO check long_url: availability and validation (delete same and home urls)
            $duplicate_record = ShortUrl::findOne(['long_url' => $model->long_url]);
            if (!empty($duplicate_record)) {
                $model = $duplicate_record;
            }
            else {
                $model->saveNewRecord();
            }
            $model->addBaseUrl();
        }
        return $this->render('index', ['model' => $model]);
    }

    public function actionParseCode()
    {
        $short_code = Yii::$app->request->get()['code'];
        //TODO validation of getting parameter
        $urlModel = ShortUrl::findOne(['short_code' => $short_code]);
        if (!empty($urlModel)) {
            return Yii::$app->response->redirect($urlModel->long_url);
        }
        return $this->render('incorrect-code', ['url_code' => $short_code]);
    }

    public function actionIncorrectCode(){
        $url = Yii::$app->request->getUrl();
        return $this->render('incorrect-code', ['url_code' => $url]);
    }

}
